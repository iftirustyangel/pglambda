'''
This file contains pg_repack kill script
ifitk
'''

from config import config
import psycopg2


# RDS Information from config.
# @Note: If using in production please use KMS to pass db credentials.

rds_host = config.get('rds_host')
rds_port = config.get('rds_port')
rds_database_name = config.get('rds_database_name')
rds_database_user = config.get('rds_database_user')
rds_database_password = config.get('rds_database_password')

print(rds_host)


def make_conn():
    try:
        conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (rds_database_name, rds_database_user, rds_host, rds_database_password))
    except Exception  as ex:
        raise ex
    return conn


def kill_pgrepack(conn):
    query = "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE state !='idle' and application_name='pg_repack'";
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    print(result)


def find_pgrepack_indexes(conn):
    query = "select relname from pg_class where oid in (select indexrelid from pg_index where indisvalid IS FALSE) and relkind ='i'";
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    print(result)
    if not result:
        return

    for line in result:
        drop_pgrepack_indexes(conn, line[0])


def drop_pgrepack_indexes(conn, index):
    query = "drop index " + index
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    print(result)


def find_pgrepack_triggers(conn):
    query = "select tgname,relname from pg_trigger , pg_class where tgname='repack_trigger' and tgrelid = pg_class.oid;";
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    print(result)
    if not result:
        return

    for line in result:
        drop_pgrepack_triggers(conn, line)


def drop_pgrepack_triggers(conn, tl):
    query = 'drop trigger ' + {tl[0]}+' on '+ {tl[1]}
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()


def find_pgrepack_tables(conn):
    query = "select relname from pg_class where relnamespace in (select oid from pg_namespace where nspname like '%pack%') and relkind = 'r'"
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    print(result)
    if not result:
        return

    for line in result:
        drop_pgrepack_tables(conn, line)


def drop_pgrepack_tables(conn, index):
    query = "drop table repack." + index
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()


def lambda_handler(event, context):
    # get a connection, if a connect cannot be made an exception will be raised here
    conn = make_conn()

    # Kill repack
    # kill_pgrepack(conn)
    # time.sleep(5*60)

    # drop repack created indexes
    find_pgrepack_indexes(conn)

    # drop repack created triggers
    find_pgrepack_triggers(conn)

    # drop repack created tables
    find_pgrepack_tables(conn)

    conn.close()


if __name__ == '__main__':
    lambda_handler(1, 2)
