# pglambda

This is sample python lambda code with psycopg2 lib for AWS lambda
 
# Runtime
PYTHON

# Edit
Add python code in lambda_function.py file and DB credentials in config.py file.

# Create lambda ZIP

```
zip -r --exclude=*venv* --exclude=*.idea* --exclude=*.git* pglambda.zip . 
```
